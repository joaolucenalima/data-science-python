import pandas as pd
import matplotlib.pyplot as plt
from openpyxl import load_workbook
from openpyxl.drawing.image import Image

quiz_df = pd.read_csv("quiz.csv")

# ----- NAME X SCORE -----
quiz_df_mean = quiz_df.groupby("quiz_name").agg({"score": "mean"})
df_name_score = quiz_df_mean.sort_values("score", ascending=False)

plt.figure(figsize=(10, 12))
plt.bar(df_name_score.index, df_name_score["score"])
plt.xticks(rotation=45, ha="right")

font_style = {"family": "sans-serif", "color": "black", "weight": "semibold", "size": 16}

plt.ylabel("Média", fontdict=font_style)

plt.savefig("namexscore.png")
plt.close()

# ----- TEMPO MÉDIO GASTO POR QUESTÃO -----
def format_points_and_duration(x):
  # 20 of 40 -> 40
  formatted_points = int(x["points"].split(" ")[2])
  x["points"] = formatted_points
  # 20s -> 20
  formatted_duration = int(x["duration"].replace("s", ""))
  x["duration"] = formatted_duration

  return x

quiz_df_formatted = quiz_df.apply(format_points_and_duration, axis=1)
quiz_df_grouped = quiz_df_formatted.groupby("quiz_name").agg({"points": "sum", "duration": "sum"})

quiz_df_grouped["average_time_per_point"] = quiz_df_grouped["duration"] / quiz_df_grouped["points"]
quiz_df_grouped = quiz_df_grouped.sort_values("average_time_per_point", ascending=False)
df_averagetime_question = quiz_df_grouped.drop(["points", "duration"], axis=1)
print(df_averagetime_question)

plt.figure(figsize=(10, 12))
plt.bar(df_averagetime_question.index, df_averagetime_question["average_time_per_point"])
plt.xticks(rotation=45, ha="right")

font_style = {"family": "sans-serif", "color": "black", "weight": "semibold", "size": 16}

plt.ylabel("Tempo", fontdict=font_style)

plt.savefig("averagetimexquestion.png")
plt.close()

# ESCREVENDO O XLSX
with pd.ExcelWriter("analise_quiz.xlsx") as writer:
  df_name_score.to_excel(writer, sheet_name="Média de pontuação", startrow=1, header=True)
  df_averagetime_question.to_excel(writer, sheet_name="Tempo médio por questão", startrow=1, header=True)

wb = load_workbook("analise_quiz.xlsx")
ws = wb["Média de pontuação"]
ws.add_image(Image("namexscore.png"), "C1")

ws = wb["Tempo médio por questão"]
ws.add_image(Image("averagetimexquestion.png"), "C1")

wb.save("analise_quiz.xlsx")